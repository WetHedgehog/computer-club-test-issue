package parseinput

import (
	"computerclub/pkg/timeconvert"
	"errors"
	"fmt"
	"log"
	"regexp"
	"slices"
	"strconv"
	"strings"
	"time"
)

func GetComputerNumber(fileInfo []string) (int, error) {
	re, _ := regexp.Compile(`\d+$`)
	if !re.Match([]byte(fileInfo[0])) {
		errorMsg := fmt.Sprintf("[err] in input file -- %s is not a computer number\n", fileInfo[0])
		return -1, errors.New(errorMsg)
	}

	computerNumber, _ := strconv.Atoi(fileInfo[0])

	if computerNumber <= 0 {
		errorMsg := fmt.Sprintf("[err] in input file -- %s -- computer number can't be negative or zero\n", fileInfo[0])
		return -1, errors.New(errorMsg)
	}

	return computerNumber, nil
}

func GetWorkTimeRange(fileInfo []string) (time.Time, time.Time, error) {
	re, _ := regexp.Compile(`\d\d:\d\d \d\d:\d\d$`)
	if !re.Match([]byte(fileInfo[1])) {
		errorMsg := fmt.Sprintf("[err] in input file -- %s is not a time line\n", fileInfo[1])
		return time.Time{}, time.Time{}, errors.New(errorMsg)
	}

	timeRange := strings.Split(fileInfo[1], " ")
	//! can be smth like 99:99 99:99
	//TODO: write checkcing
	timeStart, err := timeconvert.ConvertStr2Time(timeRange[0])
	if err != nil {
		return time.Time{}, time.Time{}, err
	}
	timeEnd, err := timeconvert.ConvertStr2Time(timeRange[1])
	if err != nil {
		return time.Time{}, time.Time{}, err
	}

	if !timeEnd.After(timeStart) {
		errorMsg := fmt.Sprintf("[err] in input file -- %s club closes before open!\n", fileInfo[1])
		return time.Time{}, time.Time{}, errors.New(errorMsg)
	}

	return timeStart, timeEnd, nil
}

func GetReward(fileInfo []string) (int, error) {
	re, _ := regexp.Compile(`\d+$`)
	if !re.Match([]byte(fileInfo[2])) {
		errorMsg := fmt.Sprintf("[err] in input file -- %s is not a reward value\n", fileInfo[2])
		return -1, errors.New(errorMsg)
	}

	reward, err := strconv.Atoi(fileInfo[2])
	if err != nil {
		log.Fatal(err)
	}

	if reward <= 0 {
		errorMsg := fmt.Sprintf("[err] in input file -- %s -- reward can't be negative or zero\n", fileInfo[2])
		return -1, errors.New(errorMsg)
	}

	return reward, nil
}

func ParseEvent(eventString string) (time.Time, int, string, error) {
	re, _ := regexp.Compile(`\d\d:\d\d \d [\S\d\s]+`)
	if !re.Match([]byte(eventString)) {
		errorMsg := fmt.Sprintf("[err] in input file -- %s is not an event string\n", eventString)
		return time.Time{}, -1, "", errors.New(errorMsg)
	}

	event := strings.Split(eventString, " ")
	curTime, err := timeconvert.ConvertStr2Time(event[0])
	if err != nil {
		return time.Time{}, -1, "", err
	}
	actioId, _ := strconv.Atoi(event[1])

	if !slices.Contains([]int{1, 2, 3, 4}, actioId) {
		errorMsg := fmt.Sprintf("[err] in input file -- %s unknown event id -- %d\n", eventString, actioId)
		return time.Time{}, -1, "", errors.New(errorMsg)
	}

	userName := event[2]

	return curTime, actioId, userName, nil
}

func GetComputerId(eventString string) (int, error) {
	re, _ := regexp.Compile(`\d\d:\d\d \d [\S\d]+ \d+`)
	if !re.Match([]byte(eventString)) {
		errorMsg := fmt.Sprintf("[err] in input file -- %s mismatch event string\n", eventString)
		return -1, errors.New(errorMsg)
	}

	event := strings.Split(eventString, " ")
	computerId, _ := strconv.Atoi(event[3])
	if computerId <= 0 {
		errorMsg := fmt.Sprintf("[err] in input file -- %s computer id can't be negative or zero\n", eventString)
		return -1, errors.New(errorMsg)
	}

	return computerId, nil
}

package parseinput

import (
	"computerclub/pkg/timeconvert"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestGetComputerNumber(t *testing.T) {
	for _, tc := range []struct {
		name     string
		data     []string
		expected int
		err      bool
	}{
		{name: "computer id", data: []string{"1"}, expected: 1, err: false},
		{name: "computer id", data: []string{"23"}, expected: 23, err: false},
		{name: "computer id", data: []string{"100000000000000"}, expected: 100000000000000, err: false},
		{name: "computer id", data: []string{"-5"}, expected: -1, err: true},
		{name: "computer id", data: []string{"0"}, expected: -1, err: true},
		{name: "computer id", data: []string{"1sdjf"}, expected: -1, err: true},
	} {
		t.Run(tc.name, func(t *testing.T) {
			computerNumber, err := GetComputerNumber(tc.data)

			if !tc.err {
				assert.Nil(t, err)
				assert.Equal(t, tc.expected, computerNumber)
			} else {
				assert.NotNil(t, err)
			}
		})
	}
}

type Event struct {
	curTime  time.Time
	eventId  int
	userName string
}

func TestParseEvent(t *testing.T) {
	tm, _ := timeconvert.ConvertStr2Time("09:48")

	for _, tc := range []struct {
		name     string
		data     string
		expected Event
		err      bool
	}{
		{name: "event", data: "09:48 1 client2", expected: Event{tm, 1, "client2"}, err: false},
		{name: "event", data: "09:48 1 client2", expected: Event{tm, 1, "client2"}, err: false},
		{name: "event", data: "09:48 1 client2", expected: Event{tm, 1, "client2"}, err: false},
		{name: "event", data: "09:48 client2", expected: Event{}, err: true},
		{name: "event", data: "09:48 -1 client2", expected: Event{}, err: true},
		{name: "event", data: "0948 -1 client2", expected: Event{}, err: true},
		{name: "event", data: "9:48 -1 client2", expected: Event{}, err: true},
		{name: "event", data: "09:48 5 client2", expected: Event{}, err: true},
	} {
		t.Run(tc.name, func(t *testing.T) {
			curTime, eventId, userName, err := ParseEvent(tc.data)

			if !tc.err {
				assert.Nil(t, err)
				assert.Equal(t, tc.expected.curTime, curTime)
				assert.Equal(t, tc.expected.eventId, eventId)
				assert.Equal(t, tc.expected.userName, userName)
			} else {
				assert.NotNil(t, err)
			}
		})
	}
}

package club

import (
	"math"
	"time"
)

//TODO: поработать с областями видимости полей классов

//===============================================Class Definition===============================================

type Computer struct {
	cash      float64 // прибыль за данным компьютером
	buisyTime float64 //  время, которое данный комп был занят
	free      bool    // индикатор, занят ли этот компьютер сейчас
}

//===============================================Public methods===============================================

func NewComputer() Computer {
	return Computer{
		cash:      0,
		buisyTime: 0,
		free:      true,
	}
}

func (c *Computer) IsFree() bool {
	return c.free
}

//===============================================Private methods===============================================

// вычисляет количество заработанных денег за интервал работы компьютера
func (c *Computer) getReward(timeStart, timeEnd time.Time, reward int) {
	diff := timeEnd.Sub(timeStart) // разница во времени
	h := diff.Hours()
	c.cash += math.Ceil(h) * float64(reward)
	c.buisyTime += h
}

func (c *Computer) markFree() {
	c.free = true
}

func (c *Computer) markBuisy() {
	c.free = false
}
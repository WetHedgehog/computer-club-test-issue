package club

import (
	queue "computerclub/pkg/queue"
	"fmt"
	"time"
)

//===============================================Class Definition===============================================

type ComputerClass struct {
	StartTime   time.Time
	EndTime     time.Time
	ComputerNum int
	reward      int

	Computers  []Computer
	Users      map[string]User // пользователи, которые на данный момент находятся в клубе (в качестве значения в мапу будем записывать номер стола, за которым он сидит, -1, если он еще не сидит ни за каким столом)
	UsersQueue queue.Queue     // очередь ожидания клиентов
}

//===============================================Public methods===============================================

func NewComputerClass(startTime time.Time,
	endTime time.Time,
	computerNum int,
	reward int) ComputerClass {
	cl := make([]Computer, 0)
	for i := 0; i < computerNum; i++ {
		cl = append(cl, NewComputer())
	}
	return ComputerClass{
		StartTime:   startTime,
		EndTime:     endTime,
		ComputerNum: computerNum,
		reward:      reward,
		Computers:   cl,
		Users:       make(map[string]User),
		UsersQueue:  queue.InitQueue(),
	}
}

func (cl *ComputerClass) LookForFreeComputer() (bool, int) {
	for computerID, computer := range cl.Computers {
		if computer.IsFree() {
			return true, computerID + 1
		}
	}
	return false, -1
}

func (cl *ComputerClass) GetReward(computerNumber int, timeStart, timeEnd time.Time) {
	cl.Computers[computerNumber-1].getReward(timeStart, timeEnd, cl.reward)
}

func (cl *ComputerClass) MarkComputerFree(computerNumber int) {
	cl.Computers[computerNumber-1].markFree()
}

func (cl *ComputerClass) MarkComputerBuisy(computerNumber int) {
	cl.Computers[computerNumber-1].markBuisy()
}

func (cl *ComputerClass) GetUserList() []string {
	userList := make([]string, 0)
	for userName, _ := range cl.Users {
		userList = append(userList, userName)
	}
	return userList
}

func (cl *ComputerClass) PrintStatistic() {
	today0 := time.Now().UTC().Truncate(24 * time.Hour)
	for computerId, computer := range cl.Computers {
		buisyTimeStr := fmt.Sprint(computer.buisyTime) + "h"
		buisyTime, _ := time.ParseDuration(buisyTimeStr)
		fmt.Printf("%d %d %s\n", computerId+1, int(computer.cash), today0.Add(buisyTime).Format("15:04"))
	}
}

//===============================================Private methods===============================================

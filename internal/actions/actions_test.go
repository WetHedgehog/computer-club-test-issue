package actions

import (
	"bytes"
	club "computerclub/internal/computerclass"
	"computerclub/internal/parseinput"
	"log"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

const baselineExample = "3\n09:00 19:00\n10"

func GenerateComputerClass() club.ComputerClass {
	fileInfo := strings.Split(baselineExample, "\n")
	computerNumber, err := parseinput.GetComputerNumber(fileInfo)
	if err != nil {
		log.Fatal(err)
	}

	timeStart, timeEnd, err := parseinput.GetWorkTimeRange(fileInfo)
	if err != nil {
		log.Fatal(err)
	}

	reward, err := parseinput.GetReward(fileInfo)
	if err != nil {
		log.Fatal(err)
	}

	cl := club.NewComputerClass(timeStart, timeEnd, computerNumber, reward)
	return cl
}

// TODO: допилить тест, какая-то проблема -- буффер шарится между тестами
func TestClientComeAction(t *testing.T) {
	for _, tc := range []struct {
		name     string
		data     string
		expected string
	}{
		{name: "test 1", data: "08:48 1 client1", expected: "08:48 1 client1\n08:48 13 NotOpenYet\n"},
		{name: "test 2", data: "09:48 1 client2", expected: "09:48 1 client2\n"},
		{name: "test 3", data: "09:48 1 client2\n09:49 1 client2", expected: "09:48 1 client2\n09:49 1 client2\n09:49 13 YouShallNotPass\n"},
	} {
		t.Run(tc.name, func(t *testing.T) {
			cl := GenerateComputerClass()
			var buf bytes.Buffer

			events := strings.Split(tc.data, "\n")
			for _, event := range events {
				curTime, _, userName, err := parseinput.ParseEvent(event)
				if err != nil {
					log.Fatal(err)
				}

				clientCome := NewClientComeAction(curTime, userName, &cl)
				clientCome.GenAction(&buf)
			}

			assert.Equal(t, tc.expected, buf.String())
		})
	}
}

func TestClientSitAction(t *testing.T) {
	cl := GenerateComputerClass()
	startEvents := []string{"09:41 1 client1", "09:48 1 client2"}
	for _, startEvent := range startEvents {
		curTime, _, userName, err := parseinput.ParseEvent(startEvent)
		if err != nil {
			log.Fatal(err)
		}

		clientCome := NewClientComeAction(curTime, userName, &cl)
		clientCome.GenAction(&bytes.Buffer{})
	}

	for _, tc := range []struct {
		name     string
		data     string
		expected string
	}{
		{name: "test 1", data: "09:54 2 client1 1", expected: "09:54 2 client1 1\n"},
		{name: "test 2", data: "10:25 2 client2 1", expected: "10:25 2 client2 1\n10:25 13 PlaceIsBusy\n"},
		{name: "test 2", data: "10:35 2 client8 3", expected: "10:35 2 client8 3\n10:35 13 ClientUnknown\n"},
	} {
		t.Run(tc.name, func(t *testing.T) {
			var buf bytes.Buffer

			events := strings.Split(tc.data, "\n")
			for _, event := range events {
				curTime, _, userName, err := parseinput.ParseEvent(event)
				if err != nil {
					log.Fatal(err)
				}
				computerId, err := parseinput.GetComputerId(event)
				if err != nil {
					log.Fatal(err)
				}

				clientSit := NewClientSitAction(curTime, userName, &cl, computerId)
				clientSit.GenAction(&buf)
			}

			assert.Equal(t, tc.expected, buf.String())
		})
	}
}

func TestClientWaitAction(t *testing.T) {
	casePreparation := func() club.ComputerClass {
		cl := GenerateComputerClass()
		startEvents := []string{
			"09:41 1 client1",
			"09:48 1 client2",
			"09:49 1 client3",
			"09:50 1 client4",
			"09:51 1 client5",
			"11:20 1 client6",
			"11:21 1 client7",
		}
		for _, startEvent := range startEvents {
			curTime, _, userName, err := parseinput.ParseEvent(startEvent)
			if err != nil {
				log.Fatal(err)
			}

			clientCome := NewClientComeAction(curTime, userName, &cl)
			clientCome.GenAction(&bytes.Buffer{})
		}
		return cl
	}

	for _, tc := range []struct {
		name     string
		data     string
		expected string
	}{
		{name: "test 1", data: "09:52 3 client1", expected: "09:52 3 client1\n09:52 13 ICanWaitNoLonger!\n"},
		{name: "test 2", data: "09:54 2 client1 1\n10:25 2 client2 2\n09:52 2 client3 3\n09:52 3 client4", expected: "09:54 2 client1 1\n10:25 2 client2 2\n09:52 2 client3 3\n09:52 3 client4\n"},
		{name: "test 3", data: "09:54 2 client1 1\n10:25 2 client2 2\n09:52 2 client3 3\n09:52 3 client4\n09:57 3 client5\n11:21 3 client6\n11:22 3 client7", expected: "09:54 2 client1 1\n10:25 2 client2 2\n09:52 2 client3 3\n09:52 3 client4\n09:57 3 client5\n11:21 3 client6\n11:22 3 client7\n11:22 11 client7\n"},
	} {
		t.Run(tc.name, func(t *testing.T) {
			var buf bytes.Buffer
			cl := casePreparation()

			events := strings.Split(tc.data, "\n")
			for _, event := range events {
				curTime, actioId, userName, err := parseinput.ParseEvent(event)
				if err != nil {
					log.Fatal(err)
				}

				switch actioId {
				case 1:
					clientCome := NewClientComeAction(curTime, userName, &cl)
					clientCome.GenAction(&buf)
				case 2:
					computerId, err := parseinput.GetComputerId(event)
					if err != nil {
						log.Fatal(err)
					}

					clientSit := NewClientSitAction(curTime, userName, &cl, computerId)
					clientSit.GenAction(&buf)
				case 3:
					clientWait := NewClientWaitAction(curTime, userName, &cl)
					clientWait.GenAction(&buf)
				case 4:
					clientLeave := NewClientLeave(curTime, userName, &cl)
					clientLeave.GenAction(&buf)
				}
			}

			assert.Equal(t, tc.expected, buf.String())
		})
	}
}

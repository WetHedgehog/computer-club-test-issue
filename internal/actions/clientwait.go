package actions

import (
	club "computerclub/internal/computerclass"
	"fmt"
	"io"
	"time"
)

type ClientWaitAction struct {
	id            int
	curTime       time.Time
	userName      string
	computerClass *club.ComputerClass
}

func NewClientWaitAction(curTime time.Time,
	userName string,
	computerClass *club.ComputerClass) ClientWaitAction {
	return ClientWaitAction{
		id:            3,
		curTime:       curTime,
		userName:      userName,
		computerClass: computerClass,
	}
}

func (a *ClientWaitAction) GenAction(w io.Writer) {
	s := fmt.Sprintf("%s %d %s\n", a.curTime.Format("15:04"), a.id, a.userName)
	fmt.Fprint(w, s)

	// прежде чем добавлять клиента в очередь ожидания, нужно проверить:
	// 1) есть ли уже свободные столы
	// 2) сколько человек в очереди на данный момент (и сравнить это количество с количеством столов)

	if ok, _ := a.computerClass.LookForFreeComputer(); ok {
		// мы хотим поставить клиента в очередь ожидания, но в клубе есть свободные столы
		defer func() {
			errorAction := NewErrorAction(a.curTime, "ICanWaitNoLonger!")
			errorAction.GenAction(w)
		}()
		return
	}

	if a.computerClass.UsersQueue.Length >= a.computerClass.ComputerNum {
		// генерируем событие 11
		defer func() {
			clientGoAway := NewClientGoAwayAction(a.curTime, a.userName, a.computerClass)
			clientGoAway.GenAction(w)
		}()
	}

	// если условия позволяют, юзера необходимо поставить в очередь ожидания
	a.computerClass.UsersQueue.Push(a.userName)
}

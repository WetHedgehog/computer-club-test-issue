package actions

import (
	club "computerclub/internal/computerclass"
	"fmt"
	"io"
	"time"
)

type ClientComeAction struct {
	id            int
	curTime       time.Time
	userName      string
	computerClass *club.ComputerClass
}

func NewClientComeAction(curTime time.Time,
	userName string,
	computerClass *club.ComputerClass) ClientComeAction {
	return ClientComeAction{
		id:            1,
		curTime:       curTime,
		userName:      userName,
		computerClass: computerClass,
	}
}

func (a *ClientComeAction) GenAction(w io.Writer) {
	s := fmt.Sprintf("%s %d %s\n", a.curTime.Format("15:04"), a.id, a.userName)
	fmt.Fprint(w, s)

	if !a.curTime.After(a.computerClass.StartTime) {
		defer func() {
			errorAction := NewErrorAction(a.curTime, "NotOpenYet")
			errorAction.GenAction(w)
		}()
		return
	}

	_, ok := a.computerClass.Users[a.userName]
	if ok {
		defer func() {
			errorAction := NewErrorAction(a.curTime, "YouShallNotPass")
			errorAction.GenAction(w)
		}()
	} else {
		a.computerClass.Users[a.userName] = club.NewUser()
	}

}

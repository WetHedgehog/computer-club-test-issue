package actions

import (
	club "computerclub/internal/computerclass"
	"fmt"
	"io"
	"time"
)

type ClientSitAction struct {
	id                int
	curTime           time.Time
	userName          string
	computerClass     *club.ComputerClass
	newComputerNumber int
}

func NewClientSitAction(curTime time.Time,
	userName string,
	computerClass *club.ComputerClass,
	newComputerNumber int) ClientSitAction {
	return ClientSitAction{
		id:                2,
		curTime:           curTime,
		userName:          userName,
		computerClass:     computerClass,
		newComputerNumber: newComputerNumber,
	}
}

func (a *ClientSitAction) GenAction(w io.Writer) {
	s := fmt.Sprintf("%s %d %s %d\n", a.curTime.Format("15:04"), a.id, a.userName, a.newComputerNumber)
	fmt.Fprint(w, s)

	user, ok := a.computerClass.Users[a.userName]
	if !ok { // неизвестный пользователь
		defer func() {
			errorAction := NewErrorAction(a.curTime, "ClientUnknown")
			errorAction.GenAction(w)
		}()
		return
	}

	// проверяем, есть ли вообще компьютер с таким номером
	if a.computerClass.ComputerNum < a.newComputerNumber {
		defer func() {
			errorAction := NewErrorAction(a.curTime, "ComputerUnknown")
			errorAction.GenAction(w)
		}()
		return
	}

	// проверяем, занят ли стол, за который хочет пересесть пользователь
	// не забываем, что компьютеры нумеруются с 1, а идексы в массиве с 0
	if !a.computerClass.Computers[a.newComputerNumber-1].IsFree() {
		// место, за которое пользователь хочет пересесть занято
		defer func() {
			errorAction := NewErrorAction(a.curTime, "PlaceIsBusy")
			errorAction.GenAction(w)
		}()
		return
	}

	// если место свободно, нужно освободить старый компьютер и занять новый

	// компьютер, за которым сидел пользователь
	oldUserComputer := user.ComputerId
	if oldUserComputer != -1 { // если пользователь уже сидел за каким-то компьютером
		// извлекаем время начала работы пользователя за помпьютером
		startUserTime := user.StartTime
		a.computerClass.GetReward(oldUserComputer, *startUserTime, a.curTime)

		// помечаем старый компьютер, как свободный
		a.computerClass.MarkComputerFree(oldUserComputer)
	}

	// устанавливаем пользователю новое время и новый компьютер
	user.StartTime = new(time.Time)
	*user.StartTime = a.curTime
	user.ComputerId = a.newComputerNumber
	a.computerClass.MarkComputerBuisy(a.newComputerNumber)

	a.computerClass.Users[a.userName] = user
}

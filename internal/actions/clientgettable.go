package actions

import (
	club "computerclub/internal/computerclass"
	"errors"
	"fmt"
	"io"
	"time"
)

type ClientGetTable struct {
	id            int
	curTime       time.Time
	computerClass *club.ComputerClass
}

func NewClientGetTable(curTime time.Time,
	computerClass *club.ComputerClass) ClientGetTable {
	return ClientGetTable{
		id:            12,
		curTime:       curTime,
		computerClass: computerClass,
	}
}

func (a *ClientGetTable) GenAction(w io.Writer) {
	userName, _ := a.computerClass.UsersQueue.Pop()
	_, computerNumber := a.computerClass.LookForFreeComputer()

	s := fmt.Sprintf("%s %d %s %d\n", a.curTime.Format("15:04"), a.id, userName, computerNumber)
	fmt.Fprint(w, s)

	user, ok := a.computerClass.Users[userName]
	if !ok {
		//TODO: переделать
		panic(errors.New("no such user"))
	}

	// устанавливаем пользователю новое время и новый компьютер
	user.StartTime = new(time.Time)
	*user.StartTime = a.curTime
	user.ComputerId = computerNumber
	a.computerClass.MarkComputerBuisy(computerNumber)

	a.computerClass.Users[userName] = user
}

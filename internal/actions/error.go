package actions

import (
	"fmt"
	"io"
	"time"
)

type ErrorAction struct {
	id        int
	errorTime time.Time
	msg       string
}

func NewErrorAction(errorTime time.Time, msg string) ErrorAction {
	return ErrorAction{
		id:        13,
		errorTime: errorTime,
		msg:       msg,
	}
}

func (a *ErrorAction) GenAction(w io.Writer) {
	s := fmt.Sprintf("%s %d %s\n", a.errorTime.Format("15:04"), a.id, a.msg)
	fmt.Fprint(w, s)
}

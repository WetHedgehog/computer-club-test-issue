package actions

import (
	club "computerclub/internal/computerclass"
	"fmt"
	"io"
	"time"
)

type ClientGoAwayAction struct {
	id            int
	curTime       time.Time
	userName      string // можем вызвать событие сразу для нескольких человек
	computerClass *club.ComputerClass
}

func NewClientGoAwayAction(curTime time.Time,
	userName string,
	computerClass *club.ComputerClass) ClientGoAwayAction {
	return ClientGoAwayAction{
		id:            11,
		curTime:       curTime,
		userName:      userName,
		computerClass: computerClass,
	}
}

func (a *ClientGoAwayAction) GenAction(w io.Writer) {
	s := fmt.Sprintf("%s %d %s\n", a.curTime.Format("15:04"), a.id, a.userName)
	fmt.Fprint(w, s)

	user, ok := a.computerClass.Users[a.userName]
	if !ok {
		defer func() {
			errorAction := NewErrorAction(a.curTime, "ClientUnknown")
			errorAction.GenAction(w)
		}()
		return
	}

	// компьютер, за которым сидел пользователь
	oldUserComputer := user.ComputerId
	if oldUserComputer != -1 { // если пользователь уже сидел за каким-то компьютером
		// извлекаем время начала работы пользователя за помпьютером
		startUserTime := user.StartTime
		a.computerClass.GetReward(oldUserComputer, *startUserTime, a.curTime)

		// помечаем старый компьютер, как свободный
		a.computerClass.MarkComputerFree(oldUserComputer)
	}

	// удаляем пользователя из мапы пользователей компьютрного клуба
	delete(a.computerClass.Users, a.userName)

	// заметим, что событие срабатывает только, если еще не конец рабочего дня
	if a.computerClass.UsersQueue.Length != 0 && a.computerClass.UsersQueue.Length < a.computerClass.ComputerNum  && a.curTime.Before(a.computerClass.EndTime) {
		// генерируем событие 12
		defer func() {
			clientGetTable := NewClientGetTable(a.curTime, a.computerClass)
			clientGetTable.GenAction(w)
		}()
	}
}

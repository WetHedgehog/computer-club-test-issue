package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

// ! Привязываемся к относительным путям -- не очень хорошо
const importPath = "../build"
const testDir = "../test"

// type TestCase struct {
// 	name     string
// 	Expected []byte
// 	Error    bool
// }

func TestBase(t *testing.T) {
	testsDataFile := filepath.Join(testDir, "test_data")

	for _, tc := range []struct {
		name     string
		Expected []byte
		Error    bool
	}{
		{
			name:     testsDataFile + "/" + "test1.txt",
			Expected: []byte("09:00\n08:48 1 client1\n08:48 13 NotOpenYet\n09:41 1 client1\n09:48 1 client2\n09:52 3 client1\n09:52 13 ICanWaitNoLonger!\n09:54 2 client1 1\n10:25 2 client2 2\n10:58 1 client3\n10:59 2 client3 3\n11:30 1 client4\n11:35 2 client4 2\n11:35 13 PlaceIsBusy\n11:45 3 client4\n12:33 4 client1\n12:33 12 client4 1\n12:43 4 client2\n15:52 4 client4\n19:00 11 client3\n19:00\n1 70 05:58\n2 30 02:18\n3 90 08:01\n"),
			Error:    false,
		},
		{
			name:     testsDataFile + "/" + "test2.txt",
			Expected: []byte("10:00\n09:59 1 Dmitriy\n09:59 13 NotOpenYet\n10:01 2 Andrey 1\n10:01 13 ClientUnknown\n10:01 1 Dmitriy\n10:02 2 Dmitriy 3\n10:02 13 ComputerUnknown\n10:03 2 Dmitriy 1\n10:03 1 Andrey\n10:04 2 Andrey 2\n11:30 1 Pavel\n11:31 3 Pavel\n11:31 1 Misha\n11:32 3 Misha\n12:21 1 Anton\n12:22 3 Anton\n12:22 11 Anton\n20:00 11 Andrey\n20:00 11 Dmitriy\n20:00 11 Misha\n20:00 11 Pavel\n20:00\n1 10 09:57\n2 10 09:56\n"),
			Error:    false,
		},
		{
			name:     testsDataFile + "/" + "test3.txt",
			Expected: []byte(""),
			Error:    true,
		},
		{
			name:     testsDataFile + "/" + "test4.txt",
			Expected: []byte(""),
			Error:    true,
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			args := []string{tc.name}

			//! Problem with hardcode absolute path
			cmd := exec.Command(importPath+"/"+"main", args...)
			cmd.Stderr = os.Stderr

			output, err := cmd.Output()
			if !tc.Error {
				require.NoError(t, err)
				CompareResults(t, tc.Expected, output)
			} else {
				require.Error(t, err)
				_, ok := err.(*exec.ExitError)
				require.True(t, ok)
			}

		})
	}
}

func CompareResults(t *testing.T, expected, actual []byte) {
	t.Helper()

	require.Equal(t, string(expected), string(actual))
}

func GetTestFiles(t *testing.T, testDirPath string) []string {
	t.Helper()

	dir, _ := os.Open(testDirPath)
	defer dir.Close()

	// Получаем список файлов и папок
	files, _ := dir.Readdir(-1)

	filesNames := make([]string, 0)
	for _, file := range files {
		fmt.Println(file.Name())
		filesNames = append(filesNames, file.Name())
	}

	return filesNames
}

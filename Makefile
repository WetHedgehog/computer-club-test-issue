.PHONY: build
build:
	rm -rf build && mkdir build  && go build -o build/main -v ./cmd/computerclub

.PHONY: gen_test_coverage
gen_test_coverage:
	go test -v ./... -coverprofile=coverage.txt
	go tool cover -html coverage.txt -o index.html

.PHONY: clear
clear:
	rm coverage.txt
	rm index.html
package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"sort"

	club "computerclub/internal/computerclass"
	"computerclub/internal/parseinput"

	inout "computerclub/pkg/inout"

	actions "computerclub/internal/actions"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("Usage %s <file_to_read>\n", os.Args[0])
		os.Exit(1)
	}

	filename := os.Args[1]

	in, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}

	r := inout.NewReader(in)
	fileInfo, err := inout.ReadAllElements(r)
	if err != nil {
		log.Fatal(err)
	}

	computerNumber, err := parseinput.GetComputerNumber(fileInfo)
	if err != nil {
		log.Fatal(err)
	}

	timeStart, timeEnd, err := parseinput.GetWorkTimeRange(fileInfo)
	if err != nil {
		log.Fatal(err)
	}

	reward, err := parseinput.GetReward(fileInfo)
	if err != nil {
		log.Fatal(err)
	}

	computerClass := club.NewComputerClass(timeStart, timeEnd, computerNumber, reward)

	fmt.Println(timeStart.Format("15:04"))

	//TODO: переписать в более приличный вид
	//! извлекаем первое событие из списка событий и фиксируем его время.
	//! Это нужно для того, чтобы убедиться, что все события идут последовательно
	firstEvent := fileInfo[3]
	prevTime, _, _, err := parseinput.ParseEvent(firstEvent)
	if err != nil {
		log.Fatal(err)
	}

	for i := 3; i < len(fileInfo); i++ {
		curTime, actioId, userName, err := parseinput.ParseEvent(fileInfo[i])
		if err != nil {
			log.Fatal(err)
		}
		if !curTime.Equal(prevTime) && !curTime.After(prevTime) { // события могут произойти в одно время (2 пользователя могут прийти одновременно)
			log.Fatal(errors.New("invalid event sequance"))
		}

		switch actioId {
		case 1:
			clientCome := actions.NewClientComeAction(curTime, userName, &computerClass)
			clientCome.GenAction(os.Stdout)
		case 2:
			computerId, err := parseinput.GetComputerId(fileInfo[i])
			if err != nil {
				log.Fatal(err)
			}

			clientSit := actions.NewClientSitAction(curTime, userName, &computerClass, computerId)
			clientSit.GenAction(os.Stdout)
		case 3:
			clientWait := actions.NewClientWaitAction(curTime, userName, &computerClass)
			clientWait.GenAction(os.Stdout)
		case 4:
			clientLeave := actions.NewClientLeave(curTime, userName, &computerClass)
			clientLeave.GenAction(os.Stdout)
		}
	}

	userList := computerClass.GetUserList()
	sort.Strings(userList) // сортируем в алфавитном порядке
	for _, userName := range userList {
		clientGoAway := actions.NewClientGoAwayAction(computerClass.EndTime, userName, &computerClass)
		clientGoAway.GenAction(os.Stdout)
	}

	fmt.Println(timeEnd.Format("15:04"))

	computerClass.PrintStatistic()
}

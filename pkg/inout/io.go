package inout

import (
	"bufio"
	"errors"
	"io"
)

type LineReader interface {
	ReadLine() (string, error)
}

type logReader struct {
	r *bufio.Reader
}

var _ LineReader = (*logReader)(nil)

func NewReader(r io.Reader) LineReader {
	return &logReader{r: bufio.NewReader(r)}
}

func readOneElement(r LineReader) (string, bool, error) {
	str, err := r.ReadLine()
	if errors.Is(err, io.EOF) {
		return str, len(str) > 0, nil
	} else if err != nil {
		return "", false, err
	} else {
		return str, true, nil
	}
}

func ReadAllElements(r LineReader) ([]string, error) {
	var values []string

	for {
		str, newItem, err := readOneElement(r)
		if err != nil {
			return values, err
		}

		if !newItem {
			break
		}

		values = append(values, str)
	}

	return values, nil
}

func (s *logReader) ReadLine() (string, error) {
	str, err := s.r.ReadString('\n')
	if err != nil {
		return str, err
	}

	return str[:len(str)-1], err
}

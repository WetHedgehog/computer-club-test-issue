package timeconvert

import (
	"errors"
	"strconv"
	"strings"
	"time"
)

func checkTimeStr(t string) error {
	tim := strings.Split(t, ":")
	hours, err := strconv.Atoi(tim[0])
	if err != nil {
		return err
	}
	minutes, err := strconv.Atoi(tim[1])
	if err != nil {
		return err
	}

	if hours < 0 || hours > 23 {
		return errors.New("miss match hours")
	}

	if minutes < 0 || minutes > 59 {
		return errors.New("miss match minutes")
	}
	return nil
}

func ConvertStr2Time(t string) (time.Time, error) {
	err := checkTimeStr(t)
	if err != nil {
		return time.Time{}, err
	}

	// I need to convert the time to a string so I can easily add the clock
	timeLayout := "2006-01-02T15:04"
	dateOnlyLayout := "2006-01-02"

	currentTime := time.Now()
	currentDate := currentTime.Format(dateOnlyLayout)

	DateStr := currentDate + "T" + t
	newTime, _ := time.Parse(timeLayout, DateStr)

	return newTime, nil
}

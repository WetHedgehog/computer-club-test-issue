package timeconvert

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestCheckTimeStr(t *testing.T) {
	for _, tc := range []struct {
		name string
		data string
		err  bool
	}{
		{name: "event", data: "09:34", err: false},
		{name: "event", data: "23:59", err: false},
		{name: "event", data: "34:34", err: true},
		{name: "event", data: "01:60", err: true},
		{name: "event", data: "aa:60", err: true},
		{name: "event", data: "12:bb", err: true},
	} {
		t.Run(tc.name, func(t *testing.T) {
			err := checkTimeStr(tc.data)

			if !tc.err {
				assert.Nil(t, err)
			} else {
				assert.NotNil(t, err)
			}
		})
	}
}

func TestConvertStr2Time(t *testing.T) {
	for _, tc := range []struct {
		name     string
		data     string
		expected time.Time
		err      bool
	}{
		{name: "event", data: "09:34", expected: time.Date(2009, time.November, 10, 9, 34, 0, 0, time.UTC), err: false},
		{name: "event", data: "23:59", expected: time.Date(2009, time.November, 10, 23, 59, 0, 0, time.UTC), err: false},
		{name: "event", data: "34:34", expected: time.Time{}, err: true},
		{name: "event", data: "01:60", expected: time.Time{}, err: true},
	} {
		t.Run(tc.name, func(t *testing.T) {
			tm, err := ConvertStr2Time(tc.data)

			if !tc.err {
				assert.Nil(t, err)

				assert.Equal(t, tm.Hour(), tc.expected.Hour())
				assert.Equal(t, tm.Minute(), tc.expected.Minute())
			} else {
				assert.NotNil(t, err)
			}
		})
	}
}

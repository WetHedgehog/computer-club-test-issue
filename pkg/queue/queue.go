package queue

import "errors"

type Queue struct {
	data   []string
	Length int
}

func InitQueue() Queue {
	return Queue{
		data:   make([]string, 0),
		Length: 0,
	}
}

func (q *Queue) Push(client string) {
	q.data = append(q.data, client)
	q.Length++
}

func (q *Queue) Pop() (string, error) {
	if q.Length == 0 {
		return "", errors.New("empty queue")
	}

	item := q.data[0]
	q.data = q.data[1:]
	q.Length--
	return item, nil
}
